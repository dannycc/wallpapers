# Wallpapers

A collection of wallpapers gathered from numerous online sources into one project.

# Ownership

If you find an image hosted in this repository that is yours and of limited use, please let me know and I will remove it.
